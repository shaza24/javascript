//excersize2.js

 /* Description of the case:
A password manager wants to create new passwords using two strings given by the user, then combined
to create a harder-to-guess combination. Given two strings, interleave the characters of the strings to
create a new string. Beginning with an empty string, alternately append a character from string a and from
string b. If one of the strings is exhausted before the other, append the remaining letters from the other
string all at once. The result is the new password.

Example
If a = 'hackerrank' and b = 'mountain', the result is hmaocuknetrariannk.

Function Description

Complete the function newPassword in the editor below.

newPassword has the following parameter(s):
 string a: the first string
 string b: the second string

 Returns:
 string: the merged string

 Constraints
1 ≤ lengths of a, b ≤ 25000
All characters in a and b are lowercase letters in the range ascii['a'-'z']

Explanation
Alternately taking characters from each string, the merged string is 'adbecf'.
*/

    var stringA = "abcdefgggggggggggggggggggg";
    var arrayA = stringA.split("");
    var stringB = "klmnop";
    var arrayB = stringB.split("");
    var arrayLength = arrayA.length + arrayB.length;
    var arrayString = [];
        
      for (let i = 0; i < arrayLength; i++){
        arrayString.push(stringA[i]);
        arrayString.push(stringB[i]);
        }    

    var newPassword = arrayString.toString().replace(/,/g, "");
    console.log(newPassword);
