/*
Consider a string, sentence, of words separated by spaces where each word is a substring consisting of
English alphabetic letters only. Find the first word in the sentence that has a length which is both an even
number and greater than or equal to the length of any other word of even length in the sentence. If there
are multiple words meeting the criteria, return the one which occurs first in the sentence.

Example
sentence = "Time to write great code"
The lengths of the words are 4, 2, 5, 5, 4, in order. The longest even length words are Time and code. The
one that occurs first is Time, the answer to return

Function Description
Complete the function longestEvenWord in the editor below.
longestEvenWord has the following parameter(s):
 string sentence: a sentence string
Returns:
 string: the word that is first occurrence of a string with maximal even number length, or the string '00'
(zero zero) if there are no even length words

Constraints
1 ≤ length of sentence ≤ 10
The sentence string consists of spaces and letters in the range ascii[a-z, A-Z, ] only.
*/

var sentence = 'ontzettender superduperlang waarom duurde dit nou zooo ontzettender ontzettend ontzettender lang';
var arrayWord = sentence.split(' ');
var evenWordNumbers = arrayWord.map(getLength);

function getLength(word){
    var isEven;
    isEven = word.length % 2;
    if (isEven == 0){
       // console.log(word.length);
        return word.length;
    } else {
        return 0;
    }
}

function getMaxNumber(maxNumber){
var maxNumber = Math.max.apply(Math, evenWordNumbers);
//console.log(maxNumber + ' maxnumber');
return maxNumber;
}

//var indexWord = evenWordNumbers.findIndex(getMaxNumber);
//console.log(indexWord);

for (let index = 0; index < evenWordNumbers.length; index++) {
    if ( evenWordNumbers[index] == getMaxNumber()){
        //console.log(evenWordNumbers[index]);
        console.log(arrayWord[index]);
        //console.log(index);
        break;
    }
}

//console.log('arrayword: ' + arrayWord);
//console.log('lengte: ' + evenWordNumbers);
//console.log(arrayWord[index]);